import { ProtectCameraConfig } from "./ProtectTypes";

export type SnapshotRequest = {
  camera: ProtectCameraConfig;
  width?: number;
  height?: number;
};
