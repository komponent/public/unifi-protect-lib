export class Headers {
  private values: any = {};

  constructor() {
    // this.values["Access-Control-Allow-Origin"] = "*";
    // this.values["User-Agent"] = "UnifiBot/1.0";
    // this.values["Accept"] = "*/*";
  }

  public has(key: string): boolean {
    return Object.keys(this.values).indexOf(key) > -1;
  }

  public get(key: string): string {
    return this.values[key];
  }

  public set(key: string, value: string) {
    this.values[key] = value;
  }

  public getHeaders() {
    return this.values;
  }
}
