import { ProtectCameraConfig } from "./ProtectTypes";

export type CameraHandlerConfig = {
  refreshInterval: number;
  onDoorbellEvent?: (camera: ProtectCameraConfig) => void;
  onMotionEvent?: (camera: ProtectCameraConfig) => void;
};
