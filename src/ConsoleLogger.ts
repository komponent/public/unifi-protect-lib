import { Logger } from "./types/Logging";

const consoleLogger: Logger = {
  info: console.info,
  warn: console.warn,
  error: console.error,
  debug: console.debug,
};

export default consoleLogger;
