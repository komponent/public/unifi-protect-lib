import { ProtectCameraConfig } from "../types/ProtectTypes";

/**
 * Utility to generate a nicely formatted device string.
 *
 * @private
 * @param {ProtectCameraConfig} camera
 * @param {*} [name=camera?.name]
 * @param {boolean} [cameraInfo=false]
 * @return {*}  {string}
 * @memberof UnifiApiClient
 */
const getDeviceName = (
  camera: ProtectCameraConfig,
  name: string = camera?.name,
  cameraInfo: boolean = false
): string => {
  // Validate our inputs.
  if (!camera) {
    return "";
  }

  // A completely enumerated device will appear as:
  // Camera [Camera Type] (address: IP address, mac: MAC address).
  return (
    name +
    " [" +
    camera.type +
    "]" +
    (cameraInfo
      ? " (address: " + camera.host + " mac: " + camera.mac + ")"
      : "")
  );
};

export default { getDeviceName };
